
OCAMLCC=ocamlfind ocamlc -package js_of_ocaml \
		-package js_of_ocaml.syntax \
		-syntax  camlp4o
#OSRC=utils.ml lexer.ml ast.ml parser.ml dot.ml solver.ml typesystem.ml dags.ml compiler.ml ptg.ml rewriting.ml
OSRC=utils.cmo lexer.cmo ast.cmo parser.cmo dot.cmo solver.cmo typesystem.cmo dags.cmo compiler.cmo ptg.cmo rewriting.cmo

.PHONY: test clean doc

doc/index.html: $(OSRC)
	ocamldoc -html -d doc $(OSRCL)

doc: doc/index.html
	open doc/index.html

tests: $(OSRC) tests.ml
	$(OCAMLCC) -g -o tests $(OSRC) tests.ml
	./tests

circuits.bytes: $(OSRC)
	$(OCAMLCC) -g -linkpkg -o $@ $(OSRC)

circuits.js: circuits.bytes
	js_of_ocaml $<

%.cmo: %.ml
	$(OCAMLCC) -c $<

#circuits: $(OSRC) circuits.ml
	#mkdir -p graphics
	#$(OCAMLCC) -g -o circuits $(OSRC) circuits.ml

clean:
	rm *.cmi
	rm *.cmo
	rm graphics/*.pdf
	rm graphics/*.dot
